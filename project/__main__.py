import json
import requests
import base64
import os
import time
import csv
import sys
import jwcrypto
import pandas as pd
import datetime as dt
import python_jwt as jwt
#from StringIO import StringIO
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
from lib_aunsight.context import AunsightContext as AUcontextMethod
from lib_aunsight.lib.util.Logger import Logger

def getConfig():
    #Give the user time to dupe the process to avoid too-rapid failure.
    time.sleep(3)
    config = {}
    missingConfig = []
    requiredConfigItems = ['AU_CONTEXT_TOKEN','AU_ORGANIZATION','AU_LOGGER_STREAM','AU_RESOURCE','atlasPrefix','adobeJWTkeySecretID','adobeJWTpayloadSecretID','adobeClientIdSecretID','adobeClientSecretSecretID','adobeURL','adobeRSIDlist','adobeCompanyID','adobeStartDate','adobeEndDate']
    optionalConfigItems = ['AU_PROJECT']
    #Acquire all required config from env
    for item in requiredConfigItems:
        if item in os.environ:
            config[item] = os.getenv(item)
        else:
            missingConfig.append(item)
    #Escape codeset as gracefully as possible w/ logging if necessary config is available.
    if len(missingConfig) > 0:
        try:
            AUcontext,localLogger = getAunsightContext(config)
            AUprint(localLogger,'fatal','Required config item(s) missing from environment:\n%s' % (', '.join(missingConfig)))
        except:
            print('Failed to create Logger after Missing Config Failure. Config items are missing:\n%s' % (', '.join(missingConfig)))
        sys.exit(2)
    #Acquire all optional config from env
    for item in optionalConfigItems:
        if item in os.environ:
            #Import list style config
            if item in ['customTags']:
                config[item] = ','.split(os.getenv(item))
            #Import string style config
            else:
                config[item] = os.getenv(item)
    #Custom Formatting
    config['adobeStartFormat'] = dt.datetime.strptime(config['adobeStartDate'],"%Y_%m_%d")
    config['adobeEndFormat'] = dt.datetime.strptime(config['adobeEndDate'],"%Y_%m_%d")
    #List Splitting
    tmpConfig = config.copy()
    for key, value in tmpConfig.items():
        if 'list' in key:
            config[key] = value.split(',')
    return config

#Gets the string value of a secret for a secretID passed.
def aunsightSecretLookup(AUcontext,secID):
    secVal = str(AUcontext.secret(secID).download().text)
    return secVal

#QA - Confirmed working properly 4/19/18  R Collins
def getAunsightContext(config):
    try:
        AUcontext = AUcontextMethod(token=config['AU_CONTEXT_TOKEN'])
    except:
        AUcontext = None
        print('Failed to build AunsightContext.  Exiting')
        sys.exit(2)
    localLogger=getAunsightLogger(config,AUcontext)
    return AUcontext,localLogger

def getAunsightLogger(config,AUcontext):
    try:
        localLogger = Logger(context=AUcontext,stream=config['AU_LOGGER_STREAM'],name='adobeDataAcquisition',throw_on_error=True,stdout=True)
    except:
        localLogger = None
        print('Failed to build Logger target.  Exiting.')
        sys.exit(2)
    return localLogger

#Accepts a Linux Epoch time and converts to a readable format
def formatFileTime(AUlogger,inString):
    try:
        newFormat = dt.datetime.strftime(dt.datetime.strptime(inString,"%a %b %d %H:%M:%S %Y"),"%Y-%m-%d %H:%M:%S")
        return newFormat
    except:
        AUprint(AUlogger,'error','Time value %s cannot be reformatted.  Skipping.' % (inString))
        return inString

#Return each day between two dates
def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + dt.timedelta(n)

#Build the name of the atlas record (in case it needs to be created)
def buildAtlasName(config,filePath):
    knownExtensions = ['psv','csv','dsv','tsv','zip','doc','xml','json','pdf','xls','xlsx','tar','gzip','gz','txt']
    pathSplit = filePath.split('/')
    if pathSplit[-1].split('.')[-1] not in knownExtensions:
        fileNoExt = pathSplit[-1]
    else:    
        fileNoExt = '_'.join(pathSplit[-1].split('.')[:-1])
    if len(pathSplit) > 0 and pathSplit[0] == '':
        del pathSplit[0]
    if len(pathSplit) > 2 and '%s%s' % ('/','/'.join(pathSplit[:-1])) != config['rootSearchPath']:
        parentDir = pathSplit[len(pathSplit)-2]
        fileName = '%s - %s' % (parentDir,fileNoExt)
    else:
        fileName = fileNoExt
    #Sanity check to be sure we don't get empty file names.
    if len(fileName) < 5:
        fileName = fileNoExt
    return fileName

#QA - Confirmed working properly 4/11/18  R Collins
def createAtlasRecord(config,AUcontext,AUlogger,fileInfo):
    tags = []
    staticTags = ['adobeSource']
    #Create list of tags from env config
    if 'customTags' in config:
        tags = staticTags + config['customTags']
    else:
        tags = staticTags
    AUprint(AUlogger,'info','Creating atlas record with data: %s' % (json.dumps(fileInfo)))
    newAtlas = AUcontext.atlas_record()
    #Setup Atlas metadata
    newAtlas.set('organization','%s' % (config['AU_ORGANIZATION']))
    newAtlas.set('name','%s' % (fileInfo['atlasName']))
    newAtlas.set('location.resource','%s' % (config['AU_RESOURCE']))
    newAtlas.set('location.path','%s' % (fileInfo['au2Path']))
    newAtlas.set('type','source')
    newAtlas.set('tags',tags)
    newAtlas.set('format','psv')
    if fileInfo['fileDelimiter']:
        newAtlas.set('delimiter',',')
    #If passed, include schema
    if 'schema' in fileInfo and fileInfo['schema'] != None:
        newAtlas.set('schema',fileInfo['schema'])
    #If a target project was provided for ownership, set PROJ Owner
    if 'AU_PROJECT' in config:
        newAtlas.set('project','%s' % (config['AU_PROJECT']))
    #Create the atlas record
    newAtlas.create()
    return newAtlas.id

#Load local pandas DF data to Aunsight
def uploadAtlasData(config,AUcontext,AUlogger,fileInfo,resultDF):
    AUprint(AUlogger,'info','Starting to write data to atlas File %s (%s).' % (fileInfo['atlasName'],fileInfo['atlasID']))
    atlasRec = AUcontext.atlas_record(fileInfo['atlasID'])
    atlasRec.upload(create_dsv(resultDF,'|').getvalue())

#Updates a processes status
def updateProcStatus(status,structured_status,AUlogger,AUcontext,config):
	try:
		AUprint(AUlogger,'info','status: %s\nstructured_status: %s' % (status,structured_status))
		AUcontext.process_job(process_job).update_status(worker_key=worker_key,status=status,structured_status=structured_status)
	except Exception as e:
		AUprint(AUlogger,'info','Failed to update Aunsight Status: %s' % (str(e)))
		return False
	return True

#Update the schema on an existing atlas record
def updateAtlasSchema(config,AUcontext,AUlogger,fileInfo):
    try:
        #Acquire active record contents
        atlasRec = AUcontext.atlas_record(fileInfo['atlasID'])
        #Update the schema on the local atlas Record
        atlasRec.set('schema',fileInfo['schema'])
        #Push the updates to Aunsight
        atlasRec.update()
        AUprint(AUlogger,'info','Updated record %s.' % (fileInfo['atlasID']))
        return True
    except:
        return False

#Gets the string value of a secret for a secretID passed.
def aunsightSecretLookup(AUcontext,secID):
    secVal = str(AUcontext.secret(secID).download().text).strip()
    return secVal

#Creates a delimited file format stream from a pandas dataframe given the provided delimiter.
def create_dsv(inputDF,delim):
    this_buf = StringIO("")
    inputDF.replace({'\n':'<br>'},regex=True).to_csv(this_buf,sep=delim,index=False,header=False)
    return this_buf

def AUprint(AUlogger,priority,message):
    if len(message) > 3500:
        message = '%s - (TRUNCATED)' % (message[:3500])
    if priority == 'info':
        AUlogger.info(message)
    elif priority == 'warn':
        AUlogger.warn(message)
    elif priority == 'trace':
        AUlogger.trace(message)
    elif priority == 'log':
        AUlogger.log(message)
    elif priority == 'fatal':
        AUlogger.fatal(message)
    elif priority == 'error':
        AUlogger.error(message)
    elif priority == 'debug':
        AUlogger.debug(message)
    else:
        AUlogger.info(message)

#Confirmed working R Collins 2020_01_29
def adobeChannelMapper(adobeRSID):
    #Add mapping swaps in format 'rsid':{'adobeBrand':'A Brand','adobeChannel':'SomeChannel'}
    return {
        'whirlpoolkitchenaidusprod':{'adobeBrand':'KitchenAid','adobeChannel':'KitchenAid.com'},
        'whirlpoolmaytagusprod':{'adobeBrand':'Maytag','adobeChannel':'Maytag.com'},
        'whirlpoolwhirlpoolusprod':{'adobeBrand':'Whirlpool','adobeChannel':'Whirlpool.com'}
    }.get(adobeRSID,{'adobeBrand':'Unknown','adobeChannel':'Unknown'})

def adobeCallWrapper(config,AUcontext,AUlogger):
    config['adobeAPIkey'] = aunsightSecretLookup(AUcontext,config['adobeClientIdSecretID'])
    currentDate = config['adobeStartFormat']
    while currentDate <= config['adobeEndFormat']:
    #for single_date in daterange(config['adobeStartFormat'],config['adobeEndFormat']):
        thisConfig = config.copy()
        thisConfig['adobeToken'] = getAdobeToken(thisConfig,AUcontext,AUlogger)
        dateString = currentDate.strftime("%Y-%m-%d")
        enddatestring = (currentDate + dt.timedelta(days=1)).strftime("%Y-%m-%d")
        thisConfig['activeStartDate'] = '%sT00:00:00.000' % (dateString)
        thisConfig['activeEndDate'] = '%sT00:00:00.000' % (enddatestring)
        allResults = pd.DataFrame()
        #Gather data for all RSIDs for this day
        for rsid in config['adobeRSIDlist']:
            thisConfig['adobeRSID'] = rsid
            AUprint(AUlogger,'info',"Beginning Calls for %s data on %s." % (thisConfig['adobeRSID'],dateString))
            #Call Adobe, receiving the post-recursion concatenation of rows in a list object.
            adobe_response_list = []
            status,callResult = callAdobe(thisConfig,adobe_response_list)
            if status:
                #Convert the list of row-based data to a pandas DF.
                return_value,parsedOutput = parseAdobeResponse(thisConfig,callResult)
                #parsedOutput = pd.DataFrame(list(parsedOutput))
            else:
                AUprint(AUlogger,'info','The Adobe API call failed for date %s with error:\n%s' % (dateString,callResult))
                sys.exit(2)
            allResults = pd.concat([allResults,parsedOutput])
        fileInfo = {}
        fileInfo['atlasName'] = 'adobeSource%s.csv' % (dateString)
        fileInfo['au2Path'] = 'adobeSource/%s.csv' % (fileInfo['atlasName'])
        fileInfo['fileDelimiter'] = ','
        fileInfo['atlasID'] = createAtlasRecord(config,AUcontext,AUlogger,fileInfo)
        if fileInfo['atlasID']:
            try:
                uploadAtlasData(config,AUcontext,AUlogger,fileInfo,allResults)
                AUprint(AUlogger,'info',"Data load successful for %s (%s)" % (fileInfo['atlasName'],fileInfo['atlasID']))
            except Exception as e:
                AUprint(AUlogger,'info',"An atlas record was created with ID %s, but the data load FAILED." % (fileInfo['atlasID']))
                raise(e)
        else:
            AUprint(AUlogger,'info',"Failed to create an atlas record for %s." % (fileInfo['atlasName']))
        if fileInfo['atlasID']:
        	try:
        		updateProcStatus('Sending Structured Data',{'Atlas_ID':fileInfo['atlasID']},AUlogger,AUcontext,config)
        		AUprint(AUlogger,'info',"Sending Structured Data for %s (%s)" % (fileInfo['atlasName'],fileInfo['atlasID']))
        	except Exception as e:
        		AUprint(AUlogger,'info',"Failed to send structured data info for atlas record for %s."  % (str(e)))
        currentDate += dt.timedelta(days=1)
    AUprint(AUlogger,'info',"Finished all API calls for all RSIDs on dates %s - %s" % (config['adobeStartDate'],config['adobeEndDate']))

#Call the Adobe API and return True,DataFrame if successful or False,'Error Message' if failed
#Confirmed working R Collins 2020_01_29
def callAdobe(config,response_list):
    adobeMetrics = [{"columnId": "0","id": "metrics/visits"},{"columnId": "1","id": "cm_visits_orders_defaultmetric"},{"columnId": "2","id": "metrics/cartadditions"},{"columnId": "4","id": "metrics/visits","filters": ["0"]},{"columnId": "6","id": "cm_visits_orders_defaultmetric","filters": ["1","0"]},{"columnId": "7","id": "metrics/cartadditions","filters": ["1","0"]},{"columnId": "8","id": "metrics/visits","filters": ["2"]},{"columnId": "9","id": "cm_visits_orders_defaultmetric","filters": ["3","2"]},{"columnId": "10","id": "metrics/cartadditions","filters": ["3","2"]},{"columnId": "11","id": "metrics/visits","filters": ["4"]},{"columnId": "12","id": "cm_visits_orders_defaultmetric","filters": ["5","4"]},{"columnId": "13","id": "metrics/cartadditions","filters": ["5","4"]},{"columnId": "14","id": "metrics/visits","filters": ["6"]},{"columnId": "15","id": "cm_visits_orders_defaultmetric","filters": ["7","6"]},{"columnId": "16","id": "metrics/cartadditions","filters": ["7","6"]}]
    adobeMetricFilters = [{"id": "0","type": "segment","segmentId": "s300002072_5d36161121d563227b057b69"},{"id": "1","type": "breakdown","dimension": "variables/product.6","itemId": "3231033346"},{"id": "2","type": "segment","segmentId": "s300002072_5d47a556d59f4a199bdfb189"},{"id": "3","type": "breakdown","dimension": "variables/product.6","itemId": "2535970543"},{"id": "4","type": "segment","segmentId": "s300002072_5d482466ff87833a7d331115"},{"id": "5","type": "breakdown","dimension": "variables/product.6","itemId": "4230010154"},{"id": "6","type": "segment","segmentId": "s300002072_5d482aea07fd425864e5b7a0"},{"id": "7","type": "breakdown","dimension": "variables/product.6","itemId": "2806790195"}]
    resultLimit = 400
    if not 'page' in config:
        config['page'] = 0
    
    payload = {
        "rsid": "%s" % (config['adobeRSID']),
        "globalFilters": [
            {
                "type": "dateRange",
                "dateRange": "%s/%s" % (config['activeStartDate'],config['activeEndDate'])
            }
        ],
        "metricContainer": {
            "metrics": adobeMetrics,
            "metricFilters": adobeMetricFilters,
        },
        "dimension": "variables/daterangeday",
        "settings": {
            "countRepeatInstances": True,
            "limit": resultLimit,
            "page": config['page'],
            "dimensionSort": "dsc",
            "nonesBehavior": "return-nones"
        },
        "statistics": {
            "functions": [
                "col-max",
                "col-min"
            ]
        }
    }
    
    headers = {
        'x-proxy-global-company-id': "%s" % (config['adobeCompanyID']),
        'x-api-key': "%s" % (config['adobeAPIkey']),
        'Content-Type': "application/json",
        'Authorization': "Bearer %s" % (config['adobeToken']),
        'Accept': "*/*",
        'Cache-Control': "no-cache",
        'Host': "analytics.adobe.io",
        'Accept-Encoding': "gzip, deflate",
        'Connection': "keep-alive",
        'cache-control': "no-cache"
    }
    
    try:
        apiResponse = requests.request("POST", config['adobeURL'], data=json.dumps(payload), headers=headers)
        apiResponseJSON = apiResponse.json()
    except Exception as e:
        return False,'The API call failed with error:\n%s' % (str(e))
    columnList = ['All_Products_Visits','All_Products_Conversion_Rate','All_Products_cartadditions','MDA_Visits','MDA_Conversion_Rate','MDA_cartadditions','SDA_Visits','SDA_Conversion_Rate','SDA_cartadditions','CPG_Visits','CPG_Conversion_Rate','CPG_cartadditions','P_A_Visits','P_A_Conversion_Rate','P_A_cartadditions']
    try:
        for row in apiResponseJSON["rows"]:
            thisRow = dict(zip(columnList,row['data']))
            thisRow['date'] = row['value']
            response_list.append(thisRow)
    except Exception as e:
        return False,'The response parser failed with error:\n%s' % (str(e))
    #Recursively call adobe API with same config, incremented page until the results are exhausted.
    if not apiResponseJSON['lastPage']:
        config['page'] += 1
        status,response_list = callAdobe(config,response_list)
    return True,response_list

def parseAdobeResponse(config,response_list):
    #Create known field lists for source and outputs
    df = pd.DataFrame(response_list)
    rsidInfo = adobeChannelMapper(config['adobeRSID'])
    df.insert(1,"Sub_Channel",rsidInfo['adobeChannel'])
    df.insert(1,"Brand",rsidInfo['adobeBrand'])
    try:
        #Separate DataFrames
        df_All_Products = df[['date','Brand','Sub_Channel','All_Products_Visits','All_Products_Conversion_Rate','All_Products_cartadditions']]
        df_MDA = df[['date','Brand','Sub_Channel','MDA_Visits','MDA_Conversion_Rate','MDA_cartadditions']]
        df_SDA = df[['date','Brand','Sub_Channel','SDA_Visits','SDA_Conversion_Rate','SDA_cartadditions']]
        df_CPG = df[['date','Brand','Sub_Channel','CPG_Visits','CPG_Conversion_Rate','CPG_cartadditions']]
        df_P_A = df[['date','Brand','Sub_Channel','P_A_Visits','P_A_Conversion_Rate','P_A_cartadditions']]
        #Rename DataFrame Fields
        df_All_Products = df_All_Products.rename(columns={"All_Products_Visits": "Visits", "All_Products_Conversion_Rate": "Conversion_Rate", "All_Products_cartadditions": "cartadditions"})
        df_MDA = df_MDA.rename(columns={"MDA_Visits": "Visits", "MDA_Conversion_Rate": "Conversion_Rate", "MDA_cartadditions": "cartadditions"})
        df_SDA = df_SDA.rename(columns={"SDA_Visits": "Visits", "SDA_Conversion_Rate": "Conversion_Rate", "SDA_cartadditions": "cartadditions"})
        df_CPG = df_CPG.rename(columns={"CPG_Visits": "Visits", "CPG_Conversion_Rate": "Conversion_Rate", "CPG_cartadditions": "cartadditions"})
        df_P_A = df_P_A.rename(columns={"P_A_Visits": "Visits", "P_A_Conversion_Rate": "Conversion_Rate", "P_A_cartadditions": "cartadditions"})
        #Insert Segment Field
        df_All_Products.insert(1,"Segment","All_Products",True)
        df_MDA.insert(1,"Segment","MDA",True)
        df_SDA.insert(1,"Segment","SDA",True)
        df_CPG.insert(1,"Segment","CPG",True)
        df_P_A.insert(1,"Segment","P&A",True)
    except Exception as e:
        return False,'The Pandas DF manipulation failed with error:\n%s' % (str(e))
    #Combine All Frames
    frames = [df_All_Products, df_MDA, df_SDA, df_CPG, df_P_A]
    df_final = pd.concat(frames)
    return True,df_final

#Confirmed working R Collins 2020_01_29
def getAdobeToken(config,AUcontext,AUlogger):
    adobeAuthURL = 'https://ims-na1.adobelogin.com/ims/exchange/jwt/'
    adobeJwtKey = aunsightSecretLookup(AUcontext,config['adobeJWTkeySecretID'])
    adobeJwtPayload = json.loads(aunsightSecretLookup(AUcontext,config['adobeJWTpayloadSecretID']))
    expDT = dt.datetime.now() + dt.timedelta(hours = 4)
    adobeJwtPayload['exp'] = expDT.strftime('%s')
    authPayload = {}
    authPayload['client_id'] = aunsightSecretLookup(AUcontext,config['adobeClientIdSecretID'])
    authPayload['client_secret'] = aunsightSecretLookup(AUcontext,config['adobeClientSecretSecretID'])
    authPayload['jwt_token'] = jwt.generate_jwt(adobeJwtPayload, jwcrypto.jwk.JWK.from_pem(adobeJwtKey.encode('ascii')), 'RS256', dt.timedelta(hours = 4))
    authResp = requests.request("POST",adobeAuthURL,data=authPayload)
    AUprint(AUlogger,'info','Retrieved new API Token from Adobe.')
    return authResp.json()['access_token']

def main():
    #Acquire job config from env
    global process_job
    global worker_key
    process_job = os.getenv("AU_PROCESS_JOB")
    worker_key = os.getenv("AU_WORKER_KEY")

    config = getConfig()
    if not config:
        AUprint(AUlogger,'info',"FAILED TO COMPLETE CONFIGURATION")
        sys.exit(2)
    #Build operational context and logger target for Aunsight
    AUcontext,AUlogger = getAunsightContext(config)
    #config['adobeToken'] = getAdobeToken(config)
    adobeCallWrapper(config,AUcontext,AUlogger)

if __name__ == '__main__':
    main()