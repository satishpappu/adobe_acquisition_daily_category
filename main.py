# Auto-generated code

import traceback
import utils
import os
logger = utils.get_logger(__name__)
context = utils.context


def main():
    # Add your code here
    logger.info("Enabled logging")
    pass

try:
    # Wrapping it around exception to catch all the errors
    # to send the logs to logger stream
    main()
except Exception as e:
    logger.error(traceback.format_exc())
    raise
