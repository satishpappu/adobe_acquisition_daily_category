# Auto-generated code

upload_process() {
    process_name=$1
    docker_image=$2
    process_id=""
    if [[ "$result" == "$process_name" ]]; then
           echo "Process already exists!"
           process_id=`au2 process list --search "^$process_name$" -R json | jq -r ".[0].id"`
    else
           process_id=`au2 process create -n "$process_name" -R json | jq -r ".id"`
           echo "Creating new process"
    fi
    echo "$process_id"
    echo "Creating new version"
    env_args=`au2 process version convert-env-file --file env-args`
    latest_version=`au2 process version create "$process_id" -R json --env "$env_args" | jq -r ".id"`
    echo "Uploading the docker image"
    docker save "$docker_image" | au2 process version upload --process "$process_id" --ver "$latest_version" --stdin --wait
    echo "Done uploading"
}

process_name=$1
docker_image=$2
organization=`au2 context show -R json | jq -r ".EFFECTIVE.organization | keys[0]"`
result=`au2 process list --search "^$process_name$" -R json | jq -r ".[0].name"`
echo "Uploading process version"
echo "Organization: ${organization} (from au2 context)"
echo "Name: ${process_name}"

read -r -p "Press y to continue [y/N] " response
case "$response" in
    [yY][eE][sS]|[yY])
        upload_process "$process_name" "$docker_image"
        ;;
    *)
        echo "Ok. exiting"
        exit 0
        ;;
esac

