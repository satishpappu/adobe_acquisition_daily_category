FROM registry.aunalytics.com/au/python_lib_aunsight:3.6-slim

LABEL maintainer spappu

# install requirements
COPY requirements.txt /requirements/
RUN pip install --upgrade pip && \
    pip install -U -r /requirements/requirements.txt

COPY . /opt/workdir

WORKDIR /opt/workdir

ENTRYPOINT ["/bin/bash"]

CMD ["script.sh"]
