NAMESPACE := aunalytics
PROJECT := analytics_catalog
DOCKER_REPO := registry.aunalytics.com
PROCESS_NAME ?= dev_adobe_acquisition
PROCESS_ID := 742e2d2d-5fa3-4066-a66e-00338e954618
CONTEXT_ID := 6efba1cc-94d0-4965-ae10-063868002181 c98dbb98-30a5-49bc-8933-9a0620982460

TAG ?= $(shell git rev-parse --short HEAD)
ifeq ($(TAG),)
TAG := latest
endif

DOCKER_IMAGE := $(DOCKER_REPO)/$(NAMESPACE)/$(PROJECT)-$(PROCESS_NAME):$(TAG)

gitfetch:
	git fetch --all 
	git reset --hard origin/master 
	git pull origin master 

createprocess:
	au2 context set -l $(CONTEXT_ID)
	$(eval ver_id=$(shell au2 process version create --process $(PROCESS_ID) -R json | python -c "import json, sys; print json.load(sys.stdin)['id']"))
	docker save $(DOCKER_IMAGE) | au2 process version upload --process $(PROCESS_ID) --ver $(ver_id) --stdin --wait

help:
	@echo "---"
	@echo "---"
	@echo "PROCESS NAME: $(PROCESS_NAME)"
	@echo "IMAGE: $(DOCKER_IMAGE)"
	@echo "make build - build the process image"
	@echo "make run - run the process locally"
	@echo "make upload - upload the process image to aunsight. Use PROCESS_NAME environment variable to update process name"
	@echo "make debug - exec into the process"

build:
	docker build -t $(DOCKER_IMAGE) .

run:
	docker run --rm \
	-it \
	--env-file env-public \
	--env-file env-args \
	-e AU_CONTEXT_TOKEN=${AU_CONTEXT_TOKEN} \
	$(DOCKER_IMAGE)

all: gitfetch build createprocess

debug:
	docker run -it --entrypoint="/bin/bash" $(DOCKER_IMAGE)
